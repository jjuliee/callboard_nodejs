import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  DeleteDateColumn,
} from 'typeorm';
import Advert from './Advert';
import Session from './Session';

@Entity()
class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  surname: string;

  @Column()
  email: string;

  @Column({ select: false })
  passw_hash: string;

  @Column({ default: false })
  is_admin: boolean;

  @OneToMany(() => Advert, (advert) => advert.user, { cascade: true })
  adverts: Advert[];

  @OneToMany(() => Session, (session) => session.user, { cascade: true })
  sessions: Session[];

  @DeleteDateColumn({ select: false })
  deleted: Date;
}

export default User;
