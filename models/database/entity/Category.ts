import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToMany,
  DeleteDateColumn,
} from 'typeorm';
import Advert from './Advert';

@Entity()
class Category {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ unique: true })
  name: string;

  @DeleteDateColumn({ select: false })
  deleted: Date;

  @ManyToMany(() => Advert, (advert) => advert.categories)
  adverts: Advert[];
}

export default Category;
