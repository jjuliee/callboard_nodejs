import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToMany,
  JoinTable,
} from 'typeorm';
import Category from './Category';
import User from './User';

@Entity()
class Advert {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: '' })
  title: string;

  @Column('text', { default: '' })
  description: string;

  @Column({ default: '' })
  picture: string;

  @Column({ default: 0 })
  price: number;

  @Column({ default: '' })
  location: string;

  @Column({ default: '' })
  phone: string;

  @Column({ default: 0 })
  views: number;

  @Column({ nullable: true })
  date_publish: Date;

  @Column({ select: false })
  is_published: boolean;

  @CreateDateColumn({ select: false })
  created: Date;

  @UpdateDateColumn({ select: false })
  updated: Date;

  @DeleteDateColumn({ select: false })
  deleted: Date;

  @ManyToOne(() => User, (user) => user.adverts)
  user: User;

  @ManyToMany(() => Category, (category) => category.adverts)
  @JoinTable()
  categories: Category[];
}

export default Advert;
