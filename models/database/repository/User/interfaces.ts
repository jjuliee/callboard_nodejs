import { IAuth } from '../../../../auth/interfaces';

export interface IPagination {
  limit: number;
  offset: number;
}

export interface IUser {
  id: number;
  name: string;
  surname: string;
  email: string;
  password: string;
  passw_hash: string;
  confirm_password: string;
  is_admin: boolean;
  //token: string;
  deleted: Date;
}

export type IUserCreateParams = Pick<
  IUser,
  'name' | 'surname' | 'email' | 'password' | 'confirm_password'
>;

export type IUserAuthParams = Pick<IUser, 'email' | 'password'>;

//export type IUserTokenAuthParams = Pick<IUser, 'token'>;

export type IUserChangeParams = {
  payload: Pick<IUser, 'id' | 'name' | 'surname' | 'email' | 'password'>;
  auth: IAuth;
};

export type IUserChangeRoleParams = Pick<IUser, 'id' | 'is_admin'>;

export type IUserFindParams = Partial<
  Pick<IUser, 'id' | 'name' | 'surname' | 'email' | 'is_admin'>
> &
  IPagination;

export type IUserDeleteParams = {
  params: Pick<IUser, 'id'>;
  auth: IAuth;
};
