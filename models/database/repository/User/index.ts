import dataSourse from '../../../../ormconfig';
import User from '../../entity/User';
import {
  IUserAuthParams,
  IUserChangeParams,
  IUserChangeRoleParams,
  IUserCreateParams,
  IUserDeleteParams,
  IUserFindParams,
} from './interfaces';
import { v4 as uuidv4 } from 'uuid';
import {
  generateHash,
  generateSaveUser,
  isEmpty,
  validateUser,
} from '../../../../common/helpers';
import * as Boom from '@hapi/boom';
import { VALIDATE_ERROR_MESSAGES } from '../../../../common/constants';
import Session from '../../entity/Session';

const UserRepository = {
  create: async (params: IUserCreateParams) => {
    const userObj = params;
    const userRepo = dataSourse.getRepository(User);

    let err = await validateUser({
      password: userObj.password,
      confirm_password: userObj.confirm_password,
      email: userObj.email,
      exist: false,
    });
    if (err) {
      return JSON.stringify(Boom.badData(err));
    }

    const user = await userRepo.save({
      name: userObj.name,
      surname: userObj.surname,
      email: userObj.email,
      passw_hash: generateHash(userObj.password),
      //token: uuidv4(),
      is_admin: false,
    });

    const response = await userRepo.findOne({
      where: { id: user.id },
    });

    if (!response) {
      return JSON.stringify(Boom.internal('Не удалось создать пользователя'));
    }
    return response;
  },

  auth: async (params: IUserAuthParams) => {
    const { password, email } = params;
    const userRepo = dataSourse.getRepository(User);
    const sessionRepo = dataSourse.getRepository(Session);

    let err = await validateUser({
      email,
      password,
      confirm_password: password,
      exist: true,
    });
    if (err) {
      return JSON.stringify(Boom.badData(err));
    }

    const existUser = await userRepo.findOne({
      where: { email: email, passw_hash: generateHash(password) },
    });

    if (!existUser) {
      return JSON.stringify(
        Boom.unauthorized(VALIDATE_ERROR_MESSAGES.errorPassword)
      );
    }

    /*const updateTokenUser = await userRepo.save({
      id: existUser.id,
      token: uuidv4(),
    });*/

    const addTokenUser = await sessionRepo.save({
      user: existUser,
      //token: uuidv4(),
    });

    if (!addTokenUser) {
      return JSON.stringify(Boom.internal('Ошибка авторизации'));
    }

    const response = await userRepo.findOne({
      where: { id: existUser.id },
    });
    return response;
  },

  /*tokenAuth: async (params: IUserTokenAuthParams) => {
    const { token } = params;
    const sessionRepo = dataSourse.getRepository(Session);

    const existSession = await sessionRepo.findOne({
      select: ['id', 'user', 'token'],
      where: { token: token },
    });
    console.log(existSession);

    if (!existSession) {
      return JSON.stringify(
        Boom.unauthorized('Сессия истекла, авторизуйтесь заново')
      );
    }
    return existSession;
  },*/

  change: async (params: IUserChangeParams) => {
    const userObj = params.payload;
    const userRepo = dataSourse.getRepository(User);

    let err = await validateUser({
      id: userObj.id,
      password: userObj.password,
      email: userObj.email,
      exist: true,
      auth_user_id: params.auth?.credentials?.user?.id,
      auth_is_admin: params.auth?.credentials?.user?.is_admin,
    });
    if (err) {
      return JSON.stringify(Boom.badData(err));
    }

    const saveObj = generateSaveUser(params);

    if (!isEmpty(saveObj)) {
      const changeUser = await userRepo.save(saveObj);
      if (!changeUser) {
        return JSON.stringify(
          Boom.unauthorized('Ошибка обновления пользователя')
        );
      }
    }

    const response = await userRepo.findOne({
      where: { id: userObj.id },
    });
    return response;
  },

  changeUserRole: async (params: IUserChangeRoleParams) => {
    const { id, is_admin } = params;
    const userRepo = dataSourse.getRepository(User);

    let err = await validateUser({
      id,
      exist: true,
    });
    if (err) {
      return JSON.stringify(Boom.badData(err));
    }

    const updateRoleUser = await userRepo.save({
      id: id,
      is_admin: is_admin,
    });

    if (!updateRoleUser) {
      return JSON.stringify(Boom.internal('Ошибка обновления пользователя'));
    }

    const response = await userRepo.findOne({
      where: { id: id },
    });
    return response;
  },

  softDelete: async (params: IUserDeleteParams) => {
    const { id } = params.params;
    const userRepo = dataSourse.getRepository(User);

    let err = await validateUser({
      id,
      exist: true,
    });
    if (err) {
      return JSON.stringify(Boom.badData(err));
    }

    const user = await userRepo.findOne({
      where: { id: id },
      relations: ['adverts', 'sessions'],
    });

    if (!user) {
      return JSON.stringify(Boom.internal('Не удалось удалить пользователя'));
    }

    const removedUser = await userRepo.softRemove(user);

    return removedUser;
  },

  find: async (params: IUserFindParams) => {
    const findObj = params;

    const conditions = dataSourse
      .createQueryBuilder()
      .select([
        'find_user.id',
        'find_user.name',
        'find_user.surname',
        'find_user.email',
        'find_user.is_admin',
      ])
      .from(User, 'find_user')
      .offset(findObj.offset)
      .limit(findObj.limit);

    if (findObj.id) {
      conditions.where('find_user.id = :id', {
        id: findObj.id,
      });
    }

    if (findObj.name) {
      conditions.andWhere('find_user.name ILIKE :name', {
        name: `%${findObj.name}%`,
      });
    }

    if (findObj.surname) {
      conditions.andWhere('find_user.surname ILIKE :surname', {
        surname: `%${findObj.surname}%`,
      });
    }

    if (findObj.email) {
      conditions.andWhere('find_user.email ILIKE :email', {
        email: `%${findObj.email}%`,
      });
    }

    if (findObj.is_admin !== undefined) {
      conditions.andWhere('find_user.is_admin = :is_admin', {
        is_admin: findObj.is_admin,
      });
    }
    //console.log(conditions.getQueryAndParameters());
    return await conditions.getMany();
  },
};

export default UserRepository;
