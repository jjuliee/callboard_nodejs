import { IAuth } from '../../../../auth/interfaces';
import { IPagination, ISort } from '../../../../common/interfaces';
import { ICategory } from '../Category/interfaces';
import { IUser } from '../User/interfaces';

/*export interface IAdvert {
  id: number;
  title: string;
  description: string;
  picture: string;
  price: number;
  location: string;
  phone: string;
  is_published: boolean;
  date_publish: Date;
  views: number;
  categories: ICategory[];
  user: IUser;
  created: Date;
  updated: Date;
  deleted: Date;
}*/

export interface IAdvertBase {
  title: string;
  description: string;
  picture: string;
  price: number;
  location: string;
  phone: string;
}

export type IAdvertCreateParams = {
  payload: IAdvertBase & { categoryIds: number[] };
  auth: IAuth;
};

export type IAdvertChangeParams = {
  payload: Partial<IAdvertBase> & { id: number; categoryIds?: number[] };
  auth: IAuth;
};

export type IAdvertChangePublishParams = {
  payload: { id: number; is_published: boolean };
  auth: IAuth;
};

export type IAdvertChangeViewsParams = { id: number };

export type IAdvertFindParams = {
  query: Partial<IAdvertBase> & {
    id?: number;
    categoryIds?: number[];
    is_published?: boolean;
    user?: IUser;
  } & IPagination &
    ISort;
  auth: IAuth;
};

export type IAdvertSoftDeleteParams = {
  params: { id: number };
  auth: IAuth;
};
