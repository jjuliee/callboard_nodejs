import dataSourse from '../../../../ormconfig';
import Advert from '../../entity/Advert';
import {
  IAdvertCreateParams,
  IAdvertChangeParams,
  IAdvertFindParams,
  IAdvertSoftDeleteParams,
  IAdvertChangePublishParams,
  IAdvertChangeViewsParams,
} from './interfaces';
import * as Boom from '@hapi/boom';
import { VALIDATE_ERROR_MESSAGES } from '../../../../common/constants';
import Category from '../../entity/Category';
import User from '../../entity/User';
import { validatePhone } from '../../../../routes/validators/_params';
import { isNumber } from '../../../../common/helpers';

const AdvertRepository = {
  create: async (params: IAdvertCreateParams) => {
    const { categoryIds, ...advertObj } = params.payload;
    const advertRepo = dataSourse.getRepository(Advert);
    const categoryRepo = dataSourse.getRepository(Category);
    const userRepo = dataSourse.getRepository(User);

    if (advertObj.phone && !validatePhone(advertObj.phone)) {
      return JSON.stringify(Boom.badData(VALIDATE_ERROR_MESSAGES.phone));
    }

    let categories: Category[] = [];
    if (categoryIds) {
      categories = await categoryRepo
        .createQueryBuilder('category')
        .where('category.id IN (:...categoryIds)', { categoryIds })
        .getMany();
    }

    if (!categories || categories.length === 0) {
      return JSON.stringify(Boom.badData(VALIDATE_ERROR_MESSAGES.category));
    }

    const authUser = await userRepo.findOne({
      where: { id: params.auth.credentials?.user?.id },
    });

    if (!authUser) {
      return JSON.stringify(Boom.badData(VALIDATE_ERROR_MESSAGES.noexistUser));
    }

    const advert = await advertRepo.save({
      title: advertObj.title,
      description: advertObj.description,
      picture: advertObj.picture,
      price: advertObj.price,
      location: advertObj.location,
      phone: advertObj.phone,
      views: 0,
      is_published: false,
      categories,
      user: authUser,
    });

    if (!advert) {
      return JSON.stringify(Boom.internal('Не удалось создать объявление'));
    }

    const response = await advertRepo.findOne({
      where: { id: advert.id },
    });
    return response;
  },

  change: async (params: IAdvertChangeParams) => {
    const { categoryIds, ...advertObj } = params.payload;
    const advertRepo = dataSourse.getRepository(Advert);
    const categoryRepo = dataSourse.getRepository(Category);

    const conditions = advertRepo
      .createQueryBuilder('advert')
      .select(['advert.id'])
      .where('advert.id = :id', { id: advertObj.id });

    if (!(await conditions.getOne())) {
      return JSON.stringify(
        Boom.badData(VALIDATE_ERROR_MESSAGES.noexistAdvert)
      );
    }

    if (params.auth?.credentials?.user?.is_admin !== true) {
      conditions.leftJoin('advert.user', 'user').andWhere('user.id = :userId', {
        userId: params.auth?.credentials?.user?.id,
      });
      if (!(await conditions.getOne())) {
        return JSON.stringify(Boom.badData(VALIDATE_ERROR_MESSAGES.norights));
      }
    }

    if (advertObj.phone && !validatePhone(advertObj.phone)) {
      return JSON.stringify(Boom.badData(VALIDATE_ERROR_MESSAGES.phone));
    }

    const categories = await categoryRepo
      .createQueryBuilder('category')
      .where('category.id IN (:...categoryIds)', { categoryIds })
      .getMany();

    if (!categories || categories.length === 0) {
      return JSON.stringify(Boom.badData(VALIDATE_ERROR_MESSAGES.category));
    }

    const advert = await advertRepo.save({
      id: advertObj.id,
      title: advertObj.title,
      description: advertObj.description,
      picture: advertObj.picture,
      price: advertObj.price,
      location: advertObj.location,
      phone: advertObj.phone,
      categories,
    });

    if (!advert) {
      return JSON.stringify(Boom.internal('Не удалось обновить объявление'));
    }

    const response = await advertRepo.findOne({
      where: { id: advert.id },
    });
    return response;
  },

  changePublish: async (params: IAdvertChangePublishParams) => {
    const { id, is_published } = params.payload;
    const advertRepo = dataSourse.getRepository(Advert);

    const conditions = advertRepo
      .createQueryBuilder('advert')
      .select(['advert.id'])
      .where('advert.id = :id', { id });

    if (!(await conditions.getOne())) {
      return JSON.stringify(
        Boom.badData(VALIDATE_ERROR_MESSAGES.noexistAdvert)
      );
    }

    if (params.auth?.credentials?.user?.is_admin !== true) {
      conditions.leftJoin('advert.user', 'user').andWhere('user.id = :userId', {
        userId: params.auth?.credentials?.user?.id,
      });
      if (!(await conditions.getOne()) || is_published === true) {
        return JSON.stringify(Boom.badData(VALIDATE_ERROR_MESSAGES.norights));
      }
    }

    const date_publish = is_published === true ? new Date() : undefined;

    const advert = await advertRepo.save({
      id,
      is_published,
      date_publish,
    });

    if (!advert) {
      return JSON.stringify(Boom.internal('Не удалось обновить объявление'));
    }

    const response = await advertRepo.findOne({
      where: { id: advert.id },
    });
    return response;
  },

  changeViews: async (params: IAdvertChangeViewsParams) => {
    const { id } = params;
    const advertRepo = dataSourse.getRepository(Advert);

    const findAdvert = await advertRepo.findOne({
      where: { id: id, is_published: true },
    });
    if (!findAdvert) {
      return JSON.stringify(
        Boom.badData(VALIDATE_ERROR_MESSAGES.noexistAdvert)
      );
    }

    const advert = await advertRepo.save({
      id,
      views: findAdvert.views + 1,
    });

    if (!advert) {
      return JSON.stringify(Boom.internal('Не удалось обновить объявление'));
    }

    const response = await advertRepo.findOne({
      where: { id: advert.id },
    });
    return response;
  },

  softDelete: async (params: IAdvertSoftDeleteParams) => {
    const { id } = params.params;
    const advertRepo = dataSourse.getRepository(Advert);

    const conditions = advertRepo
      .createQueryBuilder('advert')
      .select(['advert.id'])
      .where('advert.id = :id', { id });

    const findAdvert = await conditions.getOne();

    if (!findAdvert) {
      return JSON.stringify(
        Boom.badData(VALIDATE_ERROR_MESSAGES.noexistAdvert)
      );
    }

    if (params.auth.credentials.user?.is_admin !== true) {
      conditions.leftJoin('advert.user', 'user').andWhere('user.id = :userId', {
        userId: params.auth.credentials.user?.id,
      });
    }

    const foundAd = await conditions.getOne();
    if (!foundAd) {
      return JSON.stringify(Boom.badData(VALIDATE_ERROR_MESSAGES.norights));
    }

    const removedAdvert = await advertRepo.softDelete(foundAd);
    if (!removedAdvert) {
      return JSON.stringify(Boom.internal('Не удалось удалить объявление'));
    }
    return removedAdvert;
  },

  find: async (params: IAdvertFindParams) => {
    const findObj = params.query;
    const advertRepo = dataSourse.getRepository(Advert);

    const conditions = advertRepo
      .createQueryBuilder('find_advert')
      .offset(findObj.offset)
      .limit(findObj.limit);

    if (findObj.id) {
      conditions.where('find_advert.id = :id', {
        id: findObj.id,
      });
    }

    if (findObj.user) {
      conditions.andWhere('find_advert.user = :user', {
        user: findObj.user,
      });
    }

    if (findObj.title) {
      conditions.andWhere('find_advert.title ILIKE :title', {
        title: `%${findObj.title}%`,
      });
    }

    if (findObj.description) {
      conditions.andWhere('find_advert.description ILIKE :description', {
        description: `%${findObj.description}%`,
      });
    }

    if (isNumber(findObj.price)) {
      conditions.andWhere('find_advert.price = :price', {
        price: findObj.price,
      });
    }

    if (findObj.location) {
      conditions.andWhere('find_advert.location ILIKE :location', {
        description: `%${findObj.location}%`,
      });
    }

    if (findObj.phone) {
      conditions.andWhere('find_advert.phone = :phone', {
        phone: findObj.phone,
      });
    }

    if (
      findObj.is_published !== undefined &&
      params.auth?.credentials?.user?.is_admin === true
    ) {
      conditions.andWhere('find_advert.is_published = :is_published', {
        is_published: findObj.is_published,
      });
    }
    if (params.auth?.credentials?.user?.is_admin !== true) {
      conditions.andWhere('find_advert.is_published = :is_published', {
        is_published: true,
      });
    }

    if (findObj.categoryIds && findObj.categoryIds.length !== 0) {
      conditions.andWhere((qb) => {
        const subQuery = qb
          .subQuery()
          .select('find_advert.id')
          .from(Advert, 'find_advert')
          .leftJoin('find_advert.categories', 'category')
          .andWhere('category.id IN (:...categoryIds)', {
            categoryIds: findObj.categoryIds,
          })
          .groupBy('find_advert.id')
          .having('COUNT(find_advert.id) = :countCategories', {
            countCategories: findObj.categoryIds?.length,
          });

        return `find_advert.id IN ${subQuery.getQuery()}`;
      });
    }

    if (findObj.sortingType && findObj.sortingBy) {
      conditions.orderBy(findObj.sortingBy, findObj.sortingType, 'NULLS FIRST');
    }

    const [data, totalCount] = await Promise.all([
      conditions.getMany(),
      conditions.getCount(),
    ]);

    //console.log(conditions.getQueryAndParameters());
    return { data, totalCount };
  },
};

export default AdvertRepository;
