import { IPagination } from '../User/interfaces';

export interface ICategory {
  id: number;
  name: string;
  deleted: Date;
}

export type ICategoryFindParams = Partial<Pick<ICategory, 'id' | 'name'>> &
  IPagination;

export type ICategoryCreateParams = Pick<ICategory, 'name'>;

export type ICategoryChangeParams = Pick<ICategory, 'id' | 'name'>;

export type ICategoryDeleteParams = Pick<ICategory, 'id'>;
