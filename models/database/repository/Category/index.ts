import dataSourse from '../../../../ormconfig';
import Category from '../../entity/Category';
import * as Boom from '@hapi/boom';
import {
  ICategoryChangeParams,
  ICategoryCreateParams,
  ICategoryDeleteParams,
  ICategoryFindParams,
} from './interfaces';
import { VALIDATE_ERROR_MESSAGES } from '../../../../common/constants';

const CategoryRepository = {
  create: async (params: ICategoryCreateParams) => {
    const { name } = params;
    const categoryRepo = dataSourse.getRepository(Category);

    const findCategory = await categoryRepo.findOne({
      where: { name: name },
    });

    if (findCategory) {
      return JSON.stringify(
        Boom.badData(VALIDATE_ERROR_MESSAGES.categoryExist)
      );
    }

    const category = await categoryRepo.save({
      name,
    });

    if (!category) {
      return JSON.stringify(Boom.internal('Не удалось создать категорию'));
    }

    const response = await categoryRepo.findOne({
      where: { id: category.id },
    });
    return response;
  },

  change: async (params: ICategoryChangeParams) => {
    const { id, name } = params;
    const categoryRepo = dataSourse.getRepository(Category);

    const findCategory = await categoryRepo.findOne({
      where: { id },
    });

    if (!findCategory) {
      return JSON.stringify(Boom.badData(VALIDATE_ERROR_MESSAGES.category));
    }

    const category = await categoryRepo.save({
      id,
      name,
    });

    if (!category) {
      return JSON.stringify(Boom.internal('Не удалось обновить категорию'));
    }

    const response = await categoryRepo.findOne({
      where: { id: category.id },
    });
    return response;
  },

  find: async (params: ICategoryFindParams) => {
    const findObj = params;

    const conditions = dataSourse
      .createQueryBuilder()
      .offset(findObj.offset)
      .limit(findObj.limit)
      .select(['find_category.id', 'find_category.name'])
      .from(Category, 'find_category');

    if (findObj.id) {
      conditions.where('find_category.id = :id', {
        id: findObj.id,
      });
    }

    if (findObj.name) {
      conditions.andWhere('find_category.name ILIKE :name', {
        name: `%${findObj.name}%`,
      });
    }

    return await conditions.getMany();
  },

  softDelete: async (params: ICategoryDeleteParams) => {
    const { id } = params;
    const categoryRepo = dataSourse.getRepository(Category);

    const findCategory = await categoryRepo.findOne({
      where: { id: id },
    });

    if (!findCategory) {
      return JSON.stringify(Boom.badData(VALIDATE_ERROR_MESSAGES.category));
    }

    const removedCategory = await categoryRepo.softRemove(findCategory);
    return removedCategory;
  },
};

export default CategoryRepository;
