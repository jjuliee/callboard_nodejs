import dataSourse from '../../../../ormconfig';
import { ISessionTokenAuthParams, ISessionTokenOutParams } from './interfaces';
import * as Boom from '@hapi/boom';
import Session from '../../entity/Session';
import User from '../../entity/User';
import { VALIDATE_ERROR_MESSAGES } from '../../../../common/constants';

const SessionRepository = {
  tokenAuth: async (params: ISessionTokenAuthParams) => {
    const { token } = params;
    const sessionRepo = dataSourse.getRepository(Session);

    const existSession = await sessionRepo.findOne({
      relations: ['user'],
      select: ['id', 'user', 'token'],
      where: { token: token },
    });

    if (!existSession) {
      return JSON.stringify(
        Boom.unauthorized('Сессия истекла, авторизуйтесь заново')
      );
    }
    return existSession;
  },
  tokenOut: async (params: ISessionTokenOutParams) => {
    const { token } = params.payload;
    const sessionRepo = dataSourse.getRepository(Session);

    const authToken = params.auth.credentials.user?.token;

    let existSession: Session | null = null;

    if (token && params.auth.credentials.user?.is_admin === true) {
      existSession = await sessionRepo.findOne({
        select: ['id', 'token'],
        where: { token: token },
      });
    }
    if (token && params.auth.credentials.user?.is_admin !== true) {
      return JSON.stringify(Boom.badData(VALIDATE_ERROR_MESSAGES.norights));
    }
    if (!token && authToken) {
      existSession = await sessionRepo.findOne({
        select: ['id', 'token'],
        where: { token: authToken },
      });
    }
    if (!existSession) {
      return JSON.stringify(Boom.badData('Не удалось удалить токен'));
    }
    const removedToken = await sessionRepo.softRemove(existSession);
    return removedToken;
  },
};

export default SessionRepository;
