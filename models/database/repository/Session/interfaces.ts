import { IAuth } from '../../../../auth/interfaces';
import { IUser } from '../User/interfaces';

export interface ISession {
  id: number;
  token: string;
  user: IUser;
}

export type ISessionTokenAuthParams = Pick<ISession, 'token'>;

export type ISessionTokenOutParams = {
  payload: Partial<Pick<ISession, 'token'>>;
  auth: IAuth;
};
