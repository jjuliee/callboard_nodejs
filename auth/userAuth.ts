import Session from '../models/database/entity/Session';
import User from '../models/database/entity/User';
import dataSourse from '../ormconfig';
import { IAuth } from './interfaces';

export default function (server) {
  server.auth.strategy('user', 'bearer-access-token', {
    validate: async (request, token, h): Promise<IAuth> => {
      const userRepo = dataSourse.getRepository(User);
      const sessionRepo = dataSourse.getRepository(Session);

      const findSession = await sessionRepo.findOne({
        relations: ['user'],
        select: ['id', 'user', 'token'],
        where: { token: token },
      });

      if (!findSession) {
        return {
          isValid: false,
          credentials: {},
          artifacts: {},
        };
      }

      const authUser = await userRepo.findOne({
        select: ['id', 'is_admin'],
        where: { id: findSession?.user.id },
      });

      if (authUser) {
        return {
          isValid: true,
          credentials: { user: { token, ...authUser } },
          artifacts: {},
        };
      }
      return {
        isValid: false,
        credentials: {},
        artifacts: {},
      };
    },
  });
}
