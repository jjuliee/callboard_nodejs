export interface AuthorizationObject<
  Crds extends object = {},
  B extends boolean = boolean,
  A extends object = {}
> {
  isValid: B;
  credentials: Crds;
  artifacts?: A;
}

export interface ICredentials {
  user?: { id: number; is_admin: boolean; token: string };
}

export type IAuth = AuthorizationObject<ICredentials>;
