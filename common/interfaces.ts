import * as Hapi from '@hapi/hapi';

export interface IPagination {
  limit: number;
  offset: number;
}

export interface ISort {
  sortingType: 'ASC' | 'DESC' | undefined;
  sortingBy: string;
}

export type Decorate<T> = Readonly<T> & Hapi.Request;
