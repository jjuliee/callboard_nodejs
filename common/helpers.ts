import { createHash } from 'crypto';
import * as Boom from '@hapi/boom';
import { IUserChangeParams } from '../models/database/repository/User/interfaces';
import {
  comparePasswords,
  isExistUserById,
  isExistUserFromEmail,
  isOwnerAuth,
  validateEmail,
  validatePassword,
} from '../routes/validators';
import { VALIDATE_ERROR_MESSAGES } from './constants';

export const generateHash = (elenent: string) => {
  return createHash('md5').update(elenent).digest('hex');
};

export const showError = (e: any): string => {
  console.log(e);
  return JSON.stringify(Boom.badRequest('Непредвиденная ошибка'));
};

export const isEmpty = (obj) => {
  for (let key in obj) {
    // если тело цикла начнет выполняться - значит в объекте есть свойства
    return false;
  }
  return true;
};

export function isNumber(value: any): value is number {
  return typeof value === 'number';
}

export const validateUser = async (options: {
  id?: number;
  password?: string;
  confirm_password?: string;
  email?: string;
  exist?: boolean;
  auth_user_id?: number;
  auth_is_admin?: boolean;
}) => {
  if (
    options.id &&
    options.auth_user_id &&
    options.auth_is_admin !== undefined &&
    !isOwnerAuth(options.id, options.auth_user_id, options.auth_is_admin)
  ) {
    return VALIDATE_ERROR_MESSAGES.norights;
  }
  if (
    options.password &&
    options.confirm_password &&
    !comparePasswords(options.password, options.confirm_password)
  ) {
    return VALIDATE_ERROR_MESSAGES.comparePasswords;
  }
  if (options.password && !validatePassword(options.password)) {
    return VALIDATE_ERROR_MESSAGES.password;
  }
  if (options.exist !== true && options.password && !options.confirm_password) {
    return VALIDATE_ERROR_MESSAGES.noconfirmPassword;
  }
  if (options.email && !validateEmail(options.email)) {
    return VALIDATE_ERROR_MESSAGES.email;
  }
  if (
    options.exist === false &&
    options.email &&
    (await isExistUserFromEmail(options.email))
  ) {
    return VALIDATE_ERROR_MESSAGES.existUser;
  }
  if (
    options.exist === true &&
    options.id &&
    !(await isExistUserById(options.id))
  ) {
    return VALIDATE_ERROR_MESSAGES.noexistUser;
  }
  if (
    options.exist === true &&
    options.email &&
    !options.id &&
    !(await isExistUserFromEmail(options.email))
  ) {
    return VALIDATE_ERROR_MESSAGES.noexistEmailUser;
  }
  return null;
};

export const generateSaveUser = (params: IUserChangeParams) => {
  let saveObj = {};
  const payload = params.payload;

  Object.keys(payload).forEach((el) => {
    const canChangeUserInfo = payload[el] !== undefined;

    if (canChangeUserInfo) {
      saveObj[el] = payload[el];
    }
  });

  if (saveObj['password']) {
    saveObj['passw_hash'] = generateHash(saveObj['password']);
    delete saveObj['password'];
  }
  return saveObj;
};
