import * as Boom from '@hapi/boom';
import AdvertRepository from '../../models/database/repository/Advert';
import {
  ChangeAdvertRequest,
  ChangePublishAdvertRequest,
  ChangeViewsAdvertRequest,
  CreateAdvertRequest,
  FindAdvertRequest,
  SoftDeleteAdvertRequest,
} from './interfaces';

export default {
  createAdvert: async (request: CreateAdvertRequest) => {
    try {
      return await AdvertRepository.create({
        payload: request.payload,
        auth: request.auth,
      });
    } catch (error) {
      console.error(`AdvertRepository.create(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },

  changeAdvert: async (request: ChangeAdvertRequest) => {
    try {
      return await AdvertRepository.change({
        payload: request.payload,
        auth: request.auth,
      });
    } catch (error) {
      console.error(`AdvertRepository.change(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },

  changePublishAdvert: async (request: ChangePublishAdvertRequest) => {
    try {
      return await AdvertRepository.changePublish({
        payload: request.payload,
        auth: request.auth,
      });
    } catch (error) {
      console.error(`AdvertRepository.changePublish(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },

  changeViewsAdvert: async (request: ChangeViewsAdvertRequest) => {
    try {
      return await AdvertRepository.changeViews(request.payload);
    } catch (error) {
      console.error(`AdvertRepository.changeViews(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },

  findAdvert: async (request: FindAdvertRequest) => {
    try {
      return AdvertRepository.find({
        query: request.query,
        auth: request.auth,
      });
    } catch (error) {
      console.error(`AdvertRepository.find(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },

  softDeleteAdvert: async (request: SoftDeleteAdvertRequest) => {
    try {
      return await AdvertRepository.softDelete({
        params: request.params,
        auth: request.auth,
      });
    } catch (error) {
      console.error(`AdvertRepository.softDelete(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },
};
