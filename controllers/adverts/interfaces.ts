import { Decorate } from '../../common/interfaces';
import {
  IAdvertChangeParams,
  IAdvertChangePublishParams,
  IAdvertChangeViewsParams,
  IAdvertCreateParams,
  IAdvertFindParams,
  IAdvertSoftDeleteParams,
} from '../../models/database/repository/Advert/interfaces';

export type CreateAdvertRequest = Decorate<{
  payload: IAdvertCreateParams['payload'];
  auth: IAdvertCreateParams['auth'];
}>;

export type ChangeAdvertRequest = Decorate<{
  payload: IAdvertChangeParams['payload'];
  auth: IAdvertChangeParams['auth'];
}>;

export type ChangePublishAdvertRequest = Decorate<{
  payload: IAdvertChangePublishParams['payload'];
  auth: IAdvertChangePublishParams['auth'];
}>;

export type ChangeViewsAdvertRequest = Decorate<{
  payload: IAdvertChangeViewsParams;
}>;

export type FindAdvertRequest = Decorate<{
  query: IAdvertFindParams['query'];
  auth: IAdvertFindParams['auth'];
}>;

export type SoftDeleteAdvertRequest = Decorate<{
  params: IAdvertSoftDeleteParams['params'];
  auth: IAdvertSoftDeleteParams['auth'];
}>;
