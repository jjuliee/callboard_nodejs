import { Decorate } from '../../common/interfaces';

import {
  IUserAuthParams,
  IUserChangeParams,
  IUserChangeRoleParams,
  IUserCreateParams,
  IUserDeleteParams,
  IUserFindParams,
} from '../../models/database/repository/User/interfaces';

export type CreateUserRequest = Decorate<{
  payload: IUserCreateParams;
}>;

export type AuthUserRequest = Decorate<{
  payload: IUserAuthParams;
}>;

/*export type TokenAuthUserRequest = Decorate<{
  payload: IUserTokenAuthParams;
}>;*/

export type ChangeUserRequest = Decorate<{
  payload: IUserChangeParams['payload'];
  auth: IUserChangeParams['auth'];
}>;

export type ChangeUserRoleRequest = Decorate<{
  payload: IUserChangeRoleParams;
}>;

export type FindUserRequest = Decorate<{
  query: IUserFindParams;
}>;

export type DeleteUserRequest = Decorate<{
  params: IUserDeleteParams['params'];
  auth: IUserDeleteParams['auth'];
}>;
