import * as Boom from '@hapi/boom';
import UserRepository from '../../models/database/repository/User';
import {
  AuthUserRequest,
  ChangeUserRequest,
  ChangeUserRoleRequest,
  CreateUserRequest,
  DeleteUserRequest,
  FindUserRequest,
} from './interfaces';

export default {
  createUser: async (request: CreateUserRequest) => {
    try {
      return await UserRepository.create(request.payload);
    } catch (error) {
      console.error(`UserRepository.create(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },
  authUser: async (request: AuthUserRequest) => {
    try {
      return await UserRepository.auth(request.payload);
    } catch (error) {
      console.error(`UserRepository.auth(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },
  /*tokenAuthUser: async (request: TokenAuthUserRequest) => {
    try {
      return await UserRepository.tokenAuth(request.payload);
    } catch (error) {
      console.error(`UserRepository.tokenAuth(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },*/
  changeUser: async (request: ChangeUserRequest) => {
    try {
      return await UserRepository.change({
        payload: request.payload,
        auth: request.auth,
      });
    } catch (error) {
      console.error(`UserRepository.change(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },
  changeUserRole: async (request: ChangeUserRoleRequest) => {
    try {
      return await UserRepository.changeUserRole(request.payload);
    } catch (error) {
      console.error(`UserRepository.changeUserRole(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },
  findUser: async (request: FindUserRequest) => {
    try {
      return UserRepository.find(request.query);
    } catch (error) {
      console.error(`UserRepository.find(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },
  softDeleteUser: async (request: DeleteUserRequest) => {
    try {
      return await UserRepository.softDelete({
        params: request.params,
        auth: request.auth,
      });
    } catch (error) {
      console.error(`UserRepository.softDelete(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },
};
