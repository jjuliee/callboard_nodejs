import * as Boom from '@hapi/boom';
import CategoryRepository from '../../models/database/repository/Category';
import {
  ChangeCategoryRequest,
  CreateCategoryRequest,
  DeleteCategoryRequest,
  FindCategoryRequest,
} from './interfaces';

export default {
  createCategory: async (request: CreateCategoryRequest) => {
    try {
      return await CategoryRepository.create(request.payload);
    } catch (error) {
      console.error(`AdvertRepository.create(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },

  changeCategory: async (request: ChangeCategoryRequest) => {
    try {
      return await CategoryRepository.change(request.payload);
    } catch (error) {
      console.error(`AdvertRepository.create(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },

  findCategory: async (request: FindCategoryRequest) => {
    try {
      return CategoryRepository.find(request.query);
    } catch (error) {
      console.error(`AdvertRepository.findCategory(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },

  softDeleteCategory: async (request: DeleteCategoryRequest) => {
    try {
      return CategoryRepository.softDelete(request.params);
    } catch (error) {
      console.error(`AdvertRepository.findCategory(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },
};
