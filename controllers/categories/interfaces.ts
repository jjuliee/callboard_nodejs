import { Decorate } from '../../common/interfaces';
import {
  ICategoryChangeParams,
  ICategoryCreateParams,
  ICategoryDeleteParams,
  ICategoryFindParams,
} from '../../models/database/repository/Category/interfaces';

export type FindCategoryRequest = Decorate<{
  query: ICategoryFindParams;
}>;

export type CreateCategoryRequest = Decorate<{
  payload: ICategoryCreateParams;
}>;

export type ChangeCategoryRequest = Decorate<{
  payload: ICategoryChangeParams;
}>;

export type DeleteCategoryRequest = Decorate<{
  params: ICategoryDeleteParams;
}>;
