import * as Boom from '@hapi/boom';
import SessionRepository from '../../models/database/repository/Session';
import { TokenAuthSessionRequest, TokenOutSessionRequest } from './interfaces';

export default {
  tokenAuth: async (request: TokenAuthSessionRequest) => {
    try {
      return await SessionRepository.tokenAuth(request.payload);
    } catch (error) {
      console.error(`SessionRepository.tokenAuth(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },

  tokenOut: async (request: TokenOutSessionRequest) => {
    try {
      return await SessionRepository.tokenOut({
        payload: request.payload,
        auth: request.auth,
      });
    } catch (error) {
      console.error(`SessionRepository.tokenOut(): ${error.message}`);

      return Boom.internal(error.message);
    }
  },
};
