import { Decorate } from '../../common/interfaces';
import {
  ISessionTokenAuthParams,
  ISessionTokenOutParams,
} from '../../models/database/repository/Session/interfaces';

export type TokenAuthSessionRequest = Decorate<{
  payload: ISessionTokenAuthParams;
}>;

export type TokenOutSessionRequest = Decorate<{
  payload: ISessionTokenOutParams['payload'];
  auth: ISessionTokenOutParams['auth'];
}>;
