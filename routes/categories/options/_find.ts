import { RouteOptions } from '@hapi/hapi';
import * as Joi from 'joi';

import { makeResponsesDocs } from '../../validators';

export const findCategory: RouteOptions = {
  description: 'Получить категории',
  tags: ['api', 'category'],
  auth: { strategy: 'user' },
  validate: {
    query: Joi.object({
      id: Joi.number().example(1).description('id категории'),
      name: Joi.string().max(20).example('home').messages({
        'string.base': `category должно быть строкой`,
        'string.empty': `category не может быть пустым`,
        'string.max': `category должно быть длиной максимум {#limit} символов`,
      }),
      offset: Joi.number().description('смещение').default(0),
      limit: Joi.number().description('количество').default(10),
    }),
    failAction: async (request, h, err) => {
      //console.log(err);
      throw err;
    },
  },
  plugins: {
    'hapi-swagger': {
      '200': {
        description: 'OK',
      },
    },
  },
};
