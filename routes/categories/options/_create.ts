import { RouteOptions } from '@hapi/hapi';
import * as Joi from 'joi';

import { makeResponsesDocs } from '../../validators';

export const createCategory: RouteOptions = {
  description: 'Создание категории',
  notes: 'Маршрут создания категории',
  tags: ['api', 'category'],
  auth: { strategy: 'admin' },
  validate: {
    payload: Joi.object({
      name: Joi.string()
        .max(20)
        .required()
        .example('Спорт')
        .description('название категории')
        .messages({
          'string.base': `category должно быть строкой`,
          'string.empty': `category не может быть пустым`,
          'string.max': `category должно быть длиной максимум {#limit} символов`,
          'any.required': `Заполните обязательные поля`,
        }),
    }),

    failAction: async (request, h, err) => {
      //console.log(err);
      throw err;
    },
  },

  plugins: {
    'hapi-swagger': {
      '200': {
        description: 'OK',
      },
    },
  },
};
