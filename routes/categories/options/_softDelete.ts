import { RouteOptions } from '@hapi/hapi';
import * as Joi from 'joi';

import { makeResponsesDocs } from '../../validators';

export const softDeleteCategory: RouteOptions = {
  description: 'Удаление категории',
  notes: 'Маршрут удаления категории',
  tags: ['api', 'category'],
  auth: { strategy: 'admin' },
  validate: {
    params: Joi.object({
      id: Joi.number()
        .example(1)
        .description('id удаляемой категории')
        .required()
        .messages({
          'any.required': `Заполните обязательные поля`,
        }),
    }),

    failAction: async (request, h, err) => {
      //console.log(err);
      throw err;
    },
  },

  plugins: {
    'hapi-swagger': {
      '200': {
        description: 'OK',
      },
    },
  },
};
