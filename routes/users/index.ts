import controllers from '../../controllers/users';
import * as Hapi from '@hapi/hapi';
import * as options from './options';

const routes: Hapi.ServerRoute[] = [
  {
    method: 'POST',
    path: '/auth/registration',
    handler: controllers.createUser,
    options: options.createUser,
  },
  {
    method: 'POST',
    path: '/auth/login',
    handler: controllers.authUser,
    options: options.authUser,
  },
  {
    method: 'PUT',
    path: '/users',
    handler: controllers.changeUser,
    options: options.changeUser,
  },
  {
    method: 'PUT',
    path: '/users/role',
    handler: controllers.changeUserRole,
    options: options.changeUserRole,
  },
  {
    method: 'GET',
    path: '/users',
    handler: controllers.findUser,
    options: options.findUser,
  },
  {
    method: 'DELETE',
    path: '/users/delete/{id}',
    handler: controllers.softDeleteUser,
    options: options.softDeleteUser,
  },
];

export default routes;
