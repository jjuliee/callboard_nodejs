import { RouteOptions } from '@hapi/hapi';
import * as Joi from 'joi';

import { makeResponsesDocs } from '../../validators';

export const authUser: RouteOptions = {
  description: 'Авторизация пользователя',
  notes: 'Маршрут авторизации пользователя',
  tags: ['api', 'user'],
  validate: {
    payload: Joi.object({
      email: Joi.string().max(255).required().example('aaa@bb.cd').messages({
        'string.base': `email должно быть строкой`,
        'string.empty': `email не может быть пустым`,
        'any.required': `Заполните обязательные поля`,
      }),
      password: Joi.string()
        .max(255)
        .required()
        .min(8)
        .example('Aabcd347')
        .messages({
          'string.base': `password должно быть строкой`,
          'string.empty': `password не может быть пустым`,
          'any.required': `Заполните обязательные поля`,
        }),
    }),
    failAction: async (request, h, err) => {
      //console.log(err);
      throw err;
    },
  },

  plugins: {
    'hapi-swagger': {
      '200': {
        description: 'OK',
      },
    },
  },
};
