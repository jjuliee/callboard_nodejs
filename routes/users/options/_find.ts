import { RouteOptions } from '@hapi/hapi';
import * as Joi from 'joi';

import { makeResponsesDocs } from '../../validators';

export const findUser: RouteOptions = {
  description: 'Получить пользователей',
  notes: 'Маршрут получения пользователей',
  tags: ['api', 'user'],
  auth: { strategy: 'user' },
  validate: {
    query: Joi.object({
      id: Joi.number().example(1).description('id искомого пользователя'),
      name: Joi.string()
        .max(25)
        .example('aa')
        .description('имя пользователя')
        .messages({
          'string.base': `name должно быть строкой`,
          'string.empty': `name не может быть пустым`,
          'string.max': `name должно быть длиной максимум {#limit} символов`,
          'any.required': `Заполните обязательные поля`,
        }),
      surname: Joi.string()
        .min(1)
        .max(25)
        .example('aa')
        .description('фамилия пользователя')
        .messages({
          'string.base': `surname должно быть строкой`,
          'string.empty': `surname не может быть пустым`,
          'string.max': `surname должно быть длиной максимум {#limit} символов`,
          'any.required': `Заполните обязательные поля`,
        }),
      email: Joi.string().max(255).example('aaa@bb.cd').messages({
        'string.base': `email должно быть строкой`,
        'string.empty': `email не может быть пустым`,
        'any.required': `Заполните обязательные поля`,
      }),
      is_admin: Joi.boolean().example(false),
      offset: Joi.number().description('смещение').default(0),
      limit: Joi.number().description('количество').default(10),
    }),
    failAction: async (request, h, err) => {
      //console.log(err);
      throw err;
    },
  },
  plugins: {
    'hapi-swagger': {
      '200': {
        description: 'OK',
      },
    },
  },
};
