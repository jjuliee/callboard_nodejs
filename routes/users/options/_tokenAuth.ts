import { RouteOptions } from '@hapi/hapi';
import * as Joi from 'joi';

import { makeResponsesDocs } from '../../validators';

export const tokenAuthUser: RouteOptions = {
  description: 'Проверка сессии пользователя',
  notes: 'Маршрут проверки сессии пользователя',
  tags: ['api', 'user'],
  validate: {
    payload: Joi.object({
      token: Joi.string().max(255).required().messages({
        'string.base': `token должно быть строкой`,
        'string.empty': `token не может быть пустым`,
        'any.required': `Заполните обязательные поля`,
      }),
    }),
    failAction: async (request, h, err) => {
      //console.log(err);
      throw err;
    },
  },

  plugins: {
    'hapi-swagger': {
      '200': {
        description: 'OK',
      },
    },
  },
};
