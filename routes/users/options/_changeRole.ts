import { RouteOptions } from '@hapi/hapi';
import * as Joi from 'joi';
import { showError } from '../../../common/helpers';

import { makeResponsesDocs } from '../../validators';

export const changeUserRole: RouteOptions = {
  description: 'Редактирование роли пользователя',
  notes: 'Маршрут изменения роли пользователя',
  tags: ['api', 'user'],
  auth: { strategy: 'admin' },
  validate: {
    payload: Joi.object({
      id: Joi.number()
        .example(1)
        .description('id изеняемого пользователя')
        .required()
        .messages({
          'any.required': `Заполните обязательные поля`,
        }),
      is_admin: Joi.boolean().required().example(false),
    }),

    failAction: async (request, h, err) => {
      //console.log(err);
      throw err;
    },
  },

  plugins: {
    'hapi-swagger': {
      '200': {
        description: 'OK',
      },
    },
  },
};
