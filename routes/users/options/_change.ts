import { RouteOptions } from '@hapi/hapi';
import * as Joi from 'joi';
import { showError } from '../../../common/helpers';

import { makeResponsesDocs } from '../../validators';

export const changeUser: RouteOptions = {
  description: 'Редактирование данных пользователя',
  notes: 'Маршрут изменения пользователя',
  tags: ['api', 'user'],
  auth: { strategy: 'user' },
  validate: {
    payload: Joi.object({
      id: Joi.number()
        .example(1)
        .description('id изеняемого пользователя')
        .required()
        .messages({
          'any.required': `Заполните обязательные поля`,
        }),
      name: Joi.string()
        .max(25)
        .example('aa')
        .description('имя пользователя')
        .messages({
          'string.base': `name должно быть строкой`,
          'string.empty': `name не может быть пустым`,
          'string.max': `name должно быть длиной максимум {#limit} символов`,
          'any.required': `Заполните обязательные поля`,
        }),
      surname: Joi.string()
        .min(1)
        .max(25)
        .example('aabb')
        .description('фамилия пользователя')
        .messages({
          'string.base': `surname должно быть строкой`,
          'string.empty': `surname не может быть пустым`,
          'string.max': `surname должно быть длиной максимум {#limit} символов`,
          'any.required': `Заполните обязательные поля`,
        }),
      email: Joi.string().max(255).example('aaa@bb.cd').messages({
        'string.base': `email должно быть строкой`,
        'string.empty': `email не может быть пустым`,
        'any.required': `Заполните обязательные поля`,
      }),
      password: Joi.string().max(255).min(8).example('Aabcd347').messages({
        'string.base': `password должно быть строкой`,
        'string.empty': `password не может быть пустым`,
        'string.min': `password должно быть длиной минимум {#limit} символов`,
        'any.required': `Заполните обязательные поля`,
      }),
    }),

    failAction: async (request, h, err) => {
      //console.log(err);
      throw err;
    },
  },

  plugins: {
    'hapi-swagger': {
      '200': {
        description: 'OK',
      },
    },
  },
};
