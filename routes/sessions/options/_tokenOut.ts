import { RouteOptions } from '@hapi/hapi';
import * as Joi from 'joi';

import { makeResponsesDocs } from '../../validators';

export const tokenOut: RouteOptions = {
  description: 'Завершение сессии пользователя',
  notes: 'Маршрут завершения сессии пользователя',
  tags: ['api', 'token'],
  auth: { strategy: 'user' },
  validate: {
    payload: Joi.object({
      token: Joi.string().max(255).optional().messages({
        'string.base': `token должно быть строкой`,
        'string.empty': `token не может быть пустым`,
        'any.required': `Заполните обязательные поля`,
      }),
    }),
    failAction: async (request, h, err) => {
      //console.log(err);
      throw err;
    },
  },

  plugins: {
    'hapi-swagger': {
      '200': {
        description: 'OK',
      },
    },
  },
};
