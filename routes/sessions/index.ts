import controllers from '../../controllers/sessions';
import * as Hapi from '@hapi/hapi';
import * as options from './options';

const routes: Hapi.ServerRoute[] = [
  {
    method: 'POST',
    path: '/token',
    handler: controllers.tokenAuth,
    options: options.tokenAuth,
  },
  {
    method: 'DELETE',
    path: '/token',
    handler: controllers.tokenOut,
    options: options.tokenOut,
  },
];

export default routes;
