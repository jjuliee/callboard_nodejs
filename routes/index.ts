import users from './users';
import adverts from './adverts';
import categories from './categories';
import sessions from './sessions';

export default [...users, ...adverts, ...categories, ...sessions];
