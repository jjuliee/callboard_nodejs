import controllers from '../../controllers/adverts';
import * as Hapi from '@hapi/hapi';
import * as options from './options';

const routes: Hapi.ServerRoute[] = [
  {
    method: 'POST',
    path: '/adverts',
    handler: controllers.createAdvert,
    options: options.createAdvert,
  },
  {
    method: 'PUT',
    path: '/adverts',
    handler: controllers.changeAdvert,
    options: options.changeAdvert,
  },
  {
    method: 'PUT',
    path: '/adverts/publish',
    handler: controllers.changePublishAdvert,
    options: options.changePublishAdvert,
  },
  {
    method: 'PUT',
    path: '/adverts/show',
    handler: controllers.changeViewsAdvert,
    options: options.changeViewsAdvert,
  },
  {
    method: 'GET',
    path: '/adverts',
    handler: controllers.findAdvert,
    options: options.findAdvert,
  },
  {
    method: 'DELETE',
    path: '/adverts/delete/{id}',
    handler: controllers.softDeleteAdvert,
    options: options.softDeleteAdvert,
  },
];

export default routes;
