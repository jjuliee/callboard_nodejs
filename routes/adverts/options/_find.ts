import { RouteOptions } from '@hapi/hapi';
import * as Joi from 'joi';

import { makeResponsesDocs } from '../../validators';

export const findAdvert: RouteOptions = {
  description: 'Получить объявления',
  tags: ['api', 'advert'],
  auth: { strategy: 'user' },
  validate: {
    query: Joi.object({
      id: Joi.number().example(1).description('id искомого объявления'),
      user: Joi.number().example(1).description('id автора объявления'),
      title: Joi.string().max(225).example('Заголовок объявления').messages({
        'string.base': `title должно быть строкой`,
        'string.empty': `title не может быть пустым`,
        'string.max': `title должно быть длиной максимум {#limit} символов`,
      }),
      description: Joi.string().max(3000).example('Текст объявления').messages({
        'string.base': `description должно быть строкой`,
        'string.empty': `description не может быть пустым`,
        'string.max': `description должно быть длиной максимум {#limit} символов`,
      }),
      price: Joi.number()
        .min(0)
        .max(1000000000000000000)
        .example(100)
        .messages({
          'number.base': `price должно быть числом`,
          'number.min': `price может быть минимум {#limit}`,
          'number.max': `price может быть максимум {#limit}`,
        }),
      location: Joi.string().max(255).example('Адрес продажи товара').messages({
        'string.base': `location должно быть строкой`,
        'string.max': `location должно быть длиной максимум {#limit} символов`,
      }),
      phone: Joi.string().min(2).max(11).example('89200256652').messages({
        'string.base': `phone должно быть строкой`,
        'string.empty': `phone не может быть пустым`,
        'string.min': `phone может быть минимум {#limit} символа`,
        'string.max': `phone должно быть длиной максимум {#limit} символов`,
      }),
      is_published: Joi.boolean().example(true),
      categoryIds: Joi.array()
        .single()
        .items(Joi.number().required())
        .description('категории фильтров')
        .example(1),
      sortingType: Joi.string().valid('ASC', 'DESC').optional(),
      sortingBy: Joi.string()
        .valid('title', 'price', 'date_publish')
        .optional(),
      offset: Joi.number().description('смещение').default(0),
      limit: Joi.number().description('количество').default(10),
    }),
    failAction: async (request, h, err) => {
      //console.log(err);
      throw err;
    },
  },
  plugins: {
    'hapi-swagger': {
      '200': {
        description: 'OK',
      },
    },
  },
};
