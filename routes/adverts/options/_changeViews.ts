import { RouteOptions } from '@hapi/hapi';
import * as Joi from 'joi';
import { showError } from '../../../common/helpers';

import { makeResponsesDocs } from '../../validators';

export const changeViewsAdvert: RouteOptions = {
  description: 'Увеличение на 1 просмотров объявления',
  tags: ['api', 'advert'],
  validate: {
    payload: Joi.object({
      id: Joi.number()
        .example(1)
        .description('id изеняемого объявления')
        .required()
        .messages({
          'any.required': `Заполните обязательные поля`,
        }),
    }),

    failAction: async (request, h, err) => {
      //console.log(err);
      throw err;
    },
  },
  plugins: {
    'hapi-swagger': {
      '200': {
        description: 'OK',
      },
    },
  },
};
