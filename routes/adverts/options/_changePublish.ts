import { RouteOptions } from '@hapi/hapi';
import * as Joi from 'joi';
import { showError } from '../../../common/helpers';

import { makeResponsesDocs } from '../../validators';

export const changePublishAdvert: RouteOptions = {
  description: 'Редактирование флага публикации объявления',
  tags: ['api', 'advert'],
  auth: { strategy: 'user' },
  validate: {
    payload: Joi.object({
      id: Joi.number()
        .example(1)
        .description('id изеняемого объявления')
        .required()
        .messages({
          'any.required': `Заполните обязательные поля`,
        }),
      is_published: Joi.boolean().required().example(false),
    }),

    failAction: async (request, h, err) => {
      //console.log(err);
      throw err;
    },
  },
  plugins: {
    'hapi-swagger': {
      '200': {
        description: 'OK',
      },
    },
  },
};
