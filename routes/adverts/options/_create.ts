import { RouteOptions } from '@hapi/hapi';
import * as Joi from 'joi';

import { makeResponsesDocs } from '../../validators';

export const createAdvert: RouteOptions = {
  description: 'Создание объявления',
  tags: ['api', 'advert'],
  auth: { strategy: 'user' },
  validate: {
    payload: Joi.object({
      title: Joi.string()
        .max(225)
        .required()
        .example('Заголовок объявления')
        .messages({
          'string.base': `title должно быть строкой`,
          'string.empty': `title не может быть пустым`,
          'string.max': `title должно быть длиной максимум {#limit} символов`,
          'any.required': `Заполните обязательные поля`,
        }),
      description: Joi.string()
        .max(3000)
        .required()
        .example('Текст объявления')
        .messages({
          'string.base': `description должно быть строкой`,
          'string.empty': `description не может быть пустым`,
          'string.max': `description должно быть длиной максимум {#limit} символов`,
          'any.required': `Заполните обязательные поля`,
        }),
      picture: Joi.string()
        .required()
        .example(
          'https://montessoriself.ru/wp-content/uploads/2014/09/kartochka-yabloko-1.jpg'
        )
        .messages({
          'string.base': `picture должно быть url строкой`,
          'any.required': `Заполните обязательные поля`,
        }),
      price: Joi.number()
        .min(0)
        .max(1000000000000000000)
        .required()
        .example(100)
        .messages({
          'number.base': `price должно быть числом`,
          'number.min': `price может быть минимум {#limit}`,
          'number.max': `price может быть максимум {#limit}`,
          'any.required': `Заполните обязательные поля`,
        }),
      location: Joi.string()
        .max(255)
        .required()
        .example('Адрес продажи товара')
        .messages({
          'string.base': `location должно быть строкой`,
          'string.max': `location должно быть длиной максимум {#limit} символов`,
          'any.required': `Заполните обязательные поля`,
        }),
      phone: Joi.string()
        .min(2)
        .max(11)
        .required()
        .example('89200256652')
        .messages({
          'string.base': `phone должно быть строкой`,
          'string.empty': `phone не может быть пустым`,
          'string.min': `phone может быть минимум {#limit} символа`,
          'string.max': `phone должно быть длиной максимум {#limit} символов`,
          'any.required': `Заполните обязательные поля`,
        }),
      categoryIds: Joi.array()
        .single()
        .items(Joi.number().required())
        .description('категория объявления')
        .example(1),
    }),

    failAction: async (request, h, err) => {
      //console.log(err);
      throw err;
    },
  },

  plugins: {
    'hapi-swagger': {
      '200': {
        description: 'OK',
      },
    },
  },
};
