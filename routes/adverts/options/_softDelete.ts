import { RouteOptions } from '@hapi/hapi';
import * as Joi from 'joi';

import { makeResponsesDocs } from '../../validators';

export const softDeleteAdvert: RouteOptions = {
  description: 'Удаление объявления',
  tags: ['api', 'advert'],
  auth: { strategy: 'user' },
  validate: {
    params: Joi.object({
      id: Joi.number()
        .example(1)
        .description('id удаляемого объявления')
        .required()
        .messages({
          'any.required': `Заполните обязательные поля`,
        }),
    }),

    failAction: async (request, h, err) => {
      //console.log(err);
      throw err;
    },
  },

  plugins: {
    'hapi-swagger': {
      '200': {
        description: 'OK',
      },
    },
  },
};
