import User from '../../models/database/entity/User';
import dataSourse from '../../ormconfig';

export const validateEmail = (email: string): boolean => {
  const reg = /^[a-zA-Z]{3}@[a-zA-Z]{2}\.[a-zA-Z]{2}$/;
  if (reg.test(email) === false) {
    return false;
  }
  return true;
};

export const validatePassword = (password: string): boolean => {
  const reg = /^[A-Za-z0-9]+$/;
  if (reg.test(password) === false) {
    return false;
  }
  return true;
};

export const comparePasswords = (
  password: string,
  confirm_password: string
): boolean => {
  if (password !== confirm_password) {
    return false;
  }
  return true;
};

export const validatePhone = (phone: string): boolean => {
  const reg = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;
  if (reg.test(phone) === false) {
    return false;
  }
  return true;
};

export const isOwnerAuth = (
  user_id: number,
  auth_user_id: number,
  auth_is_admin: boolean
) => {
  if (!auth_is_admin && auth_user_id !== user_id) {
    return false;
  }
  return true;
};

export const isExistUserById = async (id: number | null) => {
  const userRepo = dataSourse.getRepository(User);

  let existUser;
  if (id) {
    existUser = await userRepo.findOne({
      select: ['id'],
      where: { id: id },
    });
  }
  if (existUser) {
    return true;
  }
  return false;
};

export const isExistUserFromEmail = async (email: string | null) => {
  const userRepo = dataSourse.getRepository(User);
  let existUser;

  if (email) {
    existUser = await userRepo.findOne({
      select: ['email'],
      where: { email: email },
    });
  }
  if (existUser) {
    return true;
  }
  return false;
};
