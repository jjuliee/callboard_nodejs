import * as Hapi from '@hapi/hapi';
import * as Inert from '@hapi/inert';
import * as Vision from '@hapi/vision';
import * as HapiSwagger from 'hapi-swagger';
import routes from './routes';
import dataSource from './ormconfig';
import userAuth from './auth/userAuth';
import adminAuth from './auth/adminAuth';
import * as AuthBearer from 'hapi-auth-bearer-token';

(async () => {
  try {
    const server = Hapi.server({
      port: 8888,
      routes: {
        cors: {
          origin: ['*'],
          additionalHeaders: [
            'Accept',
            'Authorization',
            'Content-Type',
            'If-None-Match',
            'Accept-language',
          ],
        },
      },
    });

    // add plugins
    await server.register([Inert, Vision, AuthBearer]);
    await server.register({
      plugin: HapiSwagger,
      options: {
        info: {
          title: 'API Documentation',
          description: 'API Documentation',
        },
        jsonPath: '/documentation.json',
        documentationPath: '/documentation',
        schemes: ['http', 'https'],
        debug: true,
        securityDefinitions: {
          Bearer: {
            type: 'apiKey',
            name: 'Authorization',
            description: 'Bearer token',
            in: 'header',
          },
        },
        security: [{ Bearer: [] }],
      },
    });

    //auth strategy
    userAuth(server);
    adminAuth(server);

    // routes
    server.route(routes);

    // start
    await server.start();

    console.log(
      'Server running on %s://%s:%s',
      server.info.protocol,
      server.info.address,
      server.info.port
    );

    console.log(
      'Documentation running on %s://%s:%s/documentation',
      server.info.protocol,
      server.info.address,
      server.info.port
    );

    // data sourse
    await dataSource.initialize();
    await dataSource.runMigrations();
  } catch (error) {
    console.log(error);
  }
})();

// admin 27b6e4e1-a375-4844-83a3-8c17d91c27b7
//515365e9-2bb6-44f6-af2b-0153a5d60c7f
// id 1
//"password": "Aabcd347"
// user d672421a-f349-4dbd-bd52-926bf09f0ae8
//86ce5b19-7822-4fe3-b894-cf8e71abf8dd
//id 2
